using ClayGameStudio.InputSystem;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClayGameStudio.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [TitleGroup("Properties")]
        [SerializeField] private float moveSpeed = 2.0f;

        [TitleGroup("Properties")]
        [SerializeField] private float sprintSpeed = 5.335f;

        [TitleGroup("Properties")]
        [Range(0.0f, 0.3f)]
        [SerializeField] private float rotationSmoothTime = 0.12f;

        [TitleGroup("Properties")]
        [SerializeField] private float speedChangeRate = 10.0f;

        [Space(10)]

        [TitleGroup("Properties")]
        [SerializeField] private float jumpHeight = 1.2f;

        [TitleGroup("Properties")]
        [SerializeField] private float gravity = -15.0f;

        [Space(10)]

        [TitleGroup("Properties")]
        [SerializeField] private float jumpTimeout = 0.50f;

        [TitleGroup("Properties")]
        [SerializeField] private float fallTimeout = 0.15f;

        [TitleGroup("Properties")]
        [SerializeField] private float groundedOffset = -0.14f;

        [TitleGroup("Properties")]
        [SerializeField] private float groundedRadius = 0.28f;

        [TitleGroup("Properties")]
        [SerializeField] private LayerMask groundLayers;

        [TitleGroup("Status")]
        [SerializeField] private bool grounded = true;

        [TitleGroup("Cinemachine")]
        [SerializeField] private GameObject cinemachineCameraTarget;

        [TitleGroup("Cinemachine")]
        [SerializeField] private float topClamp = 70.0f;

        [TitleGroup("Cinemachine")]
        [SerializeField] private float bottomClamp = -30.0f;

        [TitleGroup("Cinemachine")]
        [SerializeField] private float cameraAngleOverride = 0.0f;

        [TitleGroup("Cinemachine")]
        [SerializeField] private bool lockCameraPosition = false;

        // cinemachine
        private float cinemachineTargetYaw;
        private float cinemachineTargetPitch;

        // player
        private float speed;
        private float animationBlend;
        private float targetRotation = 0.0f;
        private float rotationVelocity;
        private float verticalVelocity;
        private float terminalVelocity = 53.0f;

        // timeout deltatime
        private float jumpTimeoutDelta;
        private float fallTimeoutDelta;

        // animation IDs
        private int animIDSpeed;
        private int animIDGrounded;
        private int animIDJump;
        private int animIDFreeFall;
        private int animIDMotionSpeed;

        private Animator animator;
        private CharacterController controller;
        //private StarterAssetsInputs input;
        private GameObject mainCamera;

        private const float threshold = 0.01f;

        private bool hasAnimator;

        private void Awake()
        {
            if (!mainCamera) mainCamera = Camera.main.gameObject;
        }

        private void Start()
        {
            cinemachineTargetYaw = cinemachineCameraTarget.transform.rotation.eulerAngles.y;

            hasAnimator = TryGetComponent(out animator);
            controller = GetComponent<CharacterController>();
            jumpTimeoutDelta = jumpTimeout;
            fallTimeoutDelta = fallTimeout;
        }

        private void Update()
        {
            hasAnimator = TryGetComponent(out animator);

            JumpAndGravity();
            GroundedCheck();
            Move();
        }

        private void LateUpdate()
        {
            CameraRotation();
        }

        private void JumpAndGravity()
        {
            if (grounded)
            {
                fallTimeoutDelta = fallTimeout;

                if (hasAnimator)
                {
                    
                }

                if (verticalVelocity < 0.0f)
                {
                    verticalVelocity = -2f;
                }

                if (ClayInputControl.GetButtonDown(RewiredConsts.Action.Jump) && jumpTimeoutDelta <= 0.0f)
                {
                    // the square root of H * -2 * G = how much velocity needed to reach desired height
                    verticalVelocity = Mathf.Sqrt(jumpHeight * -2f * gravity);
                    
                    if (hasAnimator)
                    {
                        
                    }
                }

                if (jumpTimeoutDelta >= 0.0f)
                {
                    jumpTimeoutDelta -= Time.deltaTime;
                }
            }
            else
            {
                jumpTimeoutDelta = jumpTimeout;

                if (fallTimeoutDelta >= 0.0f)
                {
                    fallTimeoutDelta -= Time.deltaTime;
                }
                else
                {
                    if (hasAnimator)
                    {

                    }
                }
            }

            if (verticalVelocity < terminalVelocity)
            {
                verticalVelocity += gravity * Time.deltaTime;
            }
        }

        private void GroundedCheck()
        {
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - groundedOffset,
                transform.position.z);
            grounded = Physics.CheckSphere(spherePosition, groundedRadius, groundLayers,
                QueryTriggerInteraction.Ignore);

            if (hasAnimator)
            {
                
            }
        }

        private void Move()
        {
            float targetSpeed = sprintSpeed;

            if (ClayInputControl.GetAxis(RewiredConsts.Action.MoveHorizontal, RewiredConsts.Action.MoveVertical) == Vector2.zero) targetSpeed = 0.0f;

            float currentHorizontalSpeed = new Vector3(controller.velocity.x, 0.0f, controller.velocity.z).magnitude;

            float speedOffset = 0.1f;
            float inputMagnitude = ClayInputControl.GetAxis(RewiredConsts.Action.MoveHorizontal, RewiredConsts.Action.MoveVertical).magnitude;

            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * speedChangeRate);

                speed = Mathf.Round(speed * 1000f) / 1000f;
            }
            else
            {
                speed = targetSpeed;
            }

            animationBlend = Mathf.Lerp(animationBlend, targetSpeed, Time.deltaTime * speedChangeRate);
            if (animationBlend < 0.01f) animationBlend = 0f;

            Vector3 inputDirection = new Vector3(ClayInputControl.GetAxis(RewiredConsts.Action.MoveHorizontal, RewiredConsts.Action.MoveVertical).x, 0.0f, ClayInputControl.GetAxis(RewiredConsts.Action.MoveHorizontal, RewiredConsts.Action.MoveVertical).y).normalized;

            if (ClayInputControl.GetAxis(RewiredConsts.Action.MoveHorizontal, RewiredConsts.Action.MoveVertical) != Vector2.zero)
            {
                targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                                  mainCamera.transform.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref rotationVelocity,
                    rotationSmoothTime);

                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }


            Vector3 targetDirection = Quaternion.Euler(0.0f, targetRotation, 0.0f) * Vector3.forward;

            controller.Move(targetDirection.normalized * (speed * Time.deltaTime) +
                             new Vector3(0.0f, verticalVelocity, 0.0f) * Time.deltaTime);

            if (hasAnimator)
            {
                
            }
        }

        private void CameraRotation()
        {
            /*if (input.look.sqrMagnitude >= threshold && !lockCameraPosition)
            {
                //Don't multiply mouse input by Time.deltaTime;
                float deltaTimeMultiplier = 1.0f;

                cinemachineTargetYaw += input.look.x * deltaTimeMultiplier;
                cinemachineTargetPitch += input.look.y * deltaTimeMultiplier;
            }

            // clamp our rotations so our values are limited 360 degrees
            cinemachineTargetYaw = ClampAngle(cinemachineTargetYaw, float.MinValue, float.MaxValue);
            cinemachineTargetPitch = ClampAngle(cinemachineTargetPitch, bottomClamp, topClamp);

            // Cinemachine will follow this target
            cinemachineCameraTarget.transform.rotation = Quaternion.Euler(cinemachineTargetPitch + cameraAngleOverride,
                cinemachineTargetYaw, 0.0f);*/
        }

        private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }
    }
}