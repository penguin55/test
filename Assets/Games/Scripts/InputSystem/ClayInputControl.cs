using FAPattern;
using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClayGameStudio.InputSystem
{
    public class ClayInputControl : Singleton<ClayInputControl>
    {
        private const int PLAYER_ID = 0;

        public static Rewired.Player Player
        {
            get
            {
                if (Instance) return Instance.playerInput;
                return null;
            }
        }

        private Rewired.Player _player;

        private Rewired.Player playerInput
        {
            get
            {
                if (_player == null) _player = ReInput.players.GetPlayer(PLAYER_ID);
                return _player;
            }
        }

        #region Read Input
        public static bool GetButtonDown(int actionID)
        {
            return Instance.playerInput.GetButtonDown(actionID);
        }

        public static bool GetButtonDown(string actionCommand)
        {
            return Instance.playerInput.GetButtonDown(actionCommand);
        }

        public static float GetAxis(int actionID)
        {
            return Instance.playerInput.GetAxis(actionID);
        }

        public static float GetAxis(string actionCommand)
        {
            return Instance.playerInput.GetAxis(actionCommand);
        }

        public static Vector2 GetAxis(int actionIDX, int actionIDY)
        {
            return Instance.playerInput.GetAxis2D(actionIDX, actionIDY);
        }

        public static Vector2 GetAxis(string actionCommandX, string actionCommandY)
        {
            return Instance.playerInput.GetAxis2D(actionCommandX, actionCommandY);
        }
        #endregion
    }
}