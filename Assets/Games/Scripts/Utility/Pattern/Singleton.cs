using UnityEngine;

namespace FAPattern
{
    public class Singleton<T> : MonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if (_instance == null) _instance = (T)(object) FindObjectOfType<Singleton<T>>();
                return _instance;
            }
        }
        private static T _instance;
    }
}